/* This file is part of KDevelop
    Copyright 2005 Roberto Raggi <roberto@kdevelop.org>
    Copyright 2007 Andreas Pakulat <apaku@gmx.de>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/

#ifndef KDEVPROXYSELECTIONMODEL_H
#define KDEVPROXYSELECTIONMODEL_H

#include <QItemSelectionModel> 

class QAbstractProxyModel;
class QAbstractItemView;
class QModelIndex;

class ProxySelectionModel : public QItemSelectionModel
{
        Q_OBJECT
    public:
        ProxySelectionModel( QAbstractItemView*, QItemSelectionModel*, QObject* parent = 0 );
    private slots:
        void changeCurrent( const QModelIndex&, const QModelIndex& );
        void changeCurrentColumn( const QModelIndex&, const QModelIndex& );
        void changeCurrentRow( const QModelIndex&, const QModelIndex& );
        void changeSelection( const QItemSelection&, const QItemSelection& );

        void forwardChangeCurrent( const QModelIndex&, const QModelIndex& );
        void forwardChangeCurrentColumn( const QModelIndex&, const QModelIndex& );
        void forwardChangeCurrentRow( const QModelIndex&, const QModelIndex& );
        void forwardChangeSelection( const QItemSelection&, const QItemSelection& );
    private:
        QItemSelectionModel::SelectionFlags selectionFlags() const;
        QAbstractProxyModel* proxyModel;
        QItemSelectionModel* selectionModel;
        QAbstractItemView* view;
        bool doingUpdate;
};

#endif // KDEVPROJECTMANAGER_H

