/***************************************************************************
 *   Copyright 2008 Evgeniy Ivanov <powerfox@kde.ru>                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation; either version 2 of        *
 *   the License or (at your option) version 3 or any later version        *
 *   accepted by the membership of KDE e.V. (or its successor approved     *
 *   by the membership of KDE e.V.), which shall act as a proxy            *
 *   defined in Section 14 of version 3 of the license.                    *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 ***************************************************************************/

#include "bzrplugin.h"

#include <KPluginFactory>
#include <KPluginLoader>
#include <KAboutData>
#include <klocalizedstring.h>

#include <interfaces/icore.h>

#include <vcs/dvcs/dvcsjob.h>

#include "bzrexecutor.h"


K_PLUGIN_FACTORY(KDevBzrFactory, registerPlugin<BzrPlugin>(); )
K_EXPORT_PLUGIN(KDevBzrFactory(KAboutData("kdevbzr","kdevbzr", ki18n("Bazaar Plugin"), "0.1", ki18n("Bazaar version control system support"), KAboutData::License_GPL, ki18n("Copyright 2008 Evgeniy Ivanov <powerfox@kde.ru>") ) ) )

BzrPlugin::BzrPlugin( QObject *parent, const QVariantList & )
    : DistributedVersionControlPlugin(parent, KDevBzrFactory::componentData())
{
    KDEV_USE_EXTENSION_INTERFACE( KDevelop::IBasicVersionControl )
    KDEV_USE_EXTENSION_INTERFACE( KDevelop::IDistributedVersionControl )

    core()->uiController()->addToolView(i18n("Bazaar"), DistributedVersionControlPlugin::d->m_factory);

    QString EasterEgg = i18n("It is easier to change the specification to fit the program than vice versa. It means it easier to use git than make a good DVCS from Bzr ;)");
    Q_UNUSED(EasterEgg)

    setXMLFile("kdevbzr.rc");

    DistributedVersionControlPlugin::d->m_exec = new BzrExecutor(this);
}

BzrPlugin::~BzrPlugin()
{
    delete DistributedVersionControlPlugin::d;
}

// #include "gitplugin.moc"
