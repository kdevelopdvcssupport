/***************************************************************************
 *   Copyright 2007 Robert Gruber <rgruber@users.sourceforge.net>          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __GLOBALS_H__
#define __GLOBALS_H__


#ifdef Q_WS_WIN
#define SNIPPET_END_OF_LINE "\r\n"
#else
#define SNIPPET_END_OF_LINE "\n"
#endif

#define SNIPPET_METADATA "##META##"

#endif
