#ifndef KROSSIPROBLEM_H
#define KROSSIPROBLEM_H

#include<QtCore/QVariant>

//This is file has been generated by xmltokross, you should not edit this file but the files used to generate it.

namespace KDevelop { class Problem; }
namespace Handlers
{
	QVariant _kDevelopProblemHandler(void* type);
	QVariant kDevelopProblemHandler(KDevelop::Problem* type);
	QVariant kDevelopProblemHandler(const KDevelop::Problem* type);

}

#endif
