#ifndef KROSSSYMBOLTABLE_H
#define KROSSSYMBOLTABLE_H

#include<QtCore/QVariant>

//This is file has been generated by xmltokross, you should not edit this file but the files used to generate it.

#include <language/duchain/symboltable.h>
namespace KDevelop { class KDevelop::SymbolTable; }
namespace Handlers
{
	QVariant _kDevelopSymbolTableHandler(void* type);
	QVariant kDevelopSymbolTableHandler(KDevelop::SymbolTable* type) { return _kDevelopSymbolTableHandler((void*) type); }
	QVariant kDevelopSymbolTableHandler(const KDevelop::SymbolTable* type) { return _kDevelopSymbolTableHandler((void*) type); }

}

#endif
