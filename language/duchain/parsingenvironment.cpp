/*
   Copyright 2007 David Nolden <david.nolden.kdevelop@art-master.de>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "parsingenvironment.h"
#include "topducontext.h"
#include "duchainregister.h"

namespace KDevelop
{
// REGISTER_DUCHAIN_ITEM(ParsingEnvironmentFile);

TopDUContext::Features ParsingEnvironmentFile::features() const {
  return d_func()->m_features;
}

void ParsingEnvironmentFile::setFeatures(TopDUContext::Features features) {
  d_func_dynamic()->m_features = features;
}

ParsingEnvironment::ParsingEnvironment() {
}

ParsingEnvironment::~ParsingEnvironment() {
}

ParsingEnvironmentFile::ParsingEnvironmentFile() : DUChainBase(*new ParsingEnvironmentFileData()) {
  d_func_dynamic()->setClassId(this);
}

TopDUContext* ParsingEnvironmentFile::topContext() const {
  return indexedTopContext().data();
}

ParsingEnvironmentFile::~ParsingEnvironmentFile() {
}

ParsingEnvironmentFile::ParsingEnvironmentFile(ParsingEnvironmentFileData& data) : DUChainBase(data) {
}

int ParsingEnvironment::type() const {
  return StandardParsingEnvironment;
}

int ParsingEnvironmentFile::type() const {
  return StandardParsingEnvironment;
}

bool ParsingEnvironmentFile::isProxyContext() const {
  return d_func()->m_isProxyContext;
}

void ParsingEnvironmentFile::setIsProxyContext(bool is) {
  d_func_dynamic()->m_isProxyContext = is;
}

} //KDevelop
